# Zabbix-mariadb-size

Created: Feb 11, 2021 2:41 PM
Tags: git, mssql, python

В блоке `try` указываем настройки подключения к базе данных: 

```bash
		try:
            self.con = mariadb.connect(
                    user = 'user',
                    password = 'mypass',
                    host = 'localhost',
                    port = 3306,
                    database = 'MyDB'
                    )
        except mariadb.Error as er:
            print(f"Error: {er}")
        self.cur = self.con.cursor()
```

В конфиге агента указывем пользовательские параметры:

```bash
### Option: UserParameter
#       Format: UserParameter=<key>,<shell command>
UserParameter=mariadb.size1,/opt/zabbix_scripts/db_size.py
```

На веб-морде заббикса создаем новый элемент данных. Ребутаем агента.

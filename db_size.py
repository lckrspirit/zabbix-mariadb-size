#!/bin/python3

import mariadb


class DatabaseReq():    
    def __init__(self):
        try:
            self.con = mariadb.connect(
                    user = 'user',
                    password = 'Password',
                    host = 'localhost',
                    port = 3306,
                    database = 'db_name'
                    )
        except mariadb.Error as er:
            print(f"Error: {er}")
        self.cur = self.con.cursor()


    def req(self):
        self.cur.execute('''SELECT table_schema "db_name",
                sum(data_length + index_length)/1024/1024 "size in MB"
                FROM information_schema.TABLES GROUP BY table_schema;''')
        request = self.cur.fetchall()
        print(request[0][1])

app = DatabaseReq()
app.req()
